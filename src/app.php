<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\DoctrineServiceProvider;


$app = new Silex\Application; 
$app['debug'] = true;


$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__.'/../views')
));


$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => 'ventas_online',
        'user' => 'root',
        'password' => 'ang3l./',
    )
));

return $app;

